#ifndef _KRAND_H
#define _KRAND_H

#include <stdint.h>

#define KRAND_INLINE static inline __attribute__((always_inline))

typedef uint32_t u32;
typedef uint64_t u64;

/* PRNG state */
typedef struct {
    u64 s[4];
} kstate;

/* PRNG state for 32-bit routines */
typedef struct {
    u32 s[4];
} kstate32;

/* seed with custom state */
void kseed_r(kstate* s, u64 n);
void kseed32_r(kstate32* s, u32 n);
/* gen random numbers with a seeded state */
u64 krand_r(kstate* s);
u32 krand32_r(kstate32* s);
/* gen random numbers in an interval with a seeded state */
u64 krand_range_r(kstate* s, u64 h);
u32 krand32_range_r(kstate32* s, u32 h);

/* functions that use global state */
void kseed(u64 n);
void kseed32(u32 n);
u64 krand(void);
u64 krand_range(u64 h);
u32 krand32(void);
u32 krand32_range(u32 h);

/* 64-bit floating point PRNG
 *
 * generated number is in range [0.0, 1.0)
 *
 * 32-bit float is not provided since but can be casted from a double or
 * from 1.0p-32 * (float)krand32()
 *
 * do not use the first function's state with other rand functions
 */
double krandf_r(kstate* s);
/* use this for uniform distribution */
double krandf(void);

/* sample this function for bool PRNG */
int k_bernoulli_r(kstate *s, double p);
int k_bernoulli(double p);
/* only use distribution functions if built with option 'with_dist' */
double k_chisq_r(kstate *s, int k);
double k_normal_r(kstate *s, double mean, double sd);
double k_exponential_r(kstate *s, double lambda);
int k_poisson_r(kstate *s, double lambda);
double k_gamma_r(kstate *s, double alpha, double beta);
double k_weibull_r(kstate *s, double lambda, double k);
double k_beta_r(kstate *s, double alpha, double beta);

double k_chisq(int k);
double k_normal(double mean, double sd);
double k_exponential(double lambda);
int k_poisson(double lambda);
double k_gamma(double alpha, double beta);
double k_weibull(double lambda, double k);
double k_beta(double alpha, double beta);

KRAND_INLINE u64 
lrot64(u64 x, int k)
{
	return (x << k) | (x >> (64 - k));
}

KRAND_INLINE u32 
lrot32(u32 x, int k)
{
	return (x << k) | (x >> (64 - k));
}

KRAND_INLINE u64 
rrot64(u64 x, int k)
{
	return (x >> k) | (x << (64 - k));
}

KRAND_INLINE u32 
rrot32(u32 x, int k)
{
	return (x >> k) | (x << (64 - k));
}

#endif /* _KRAND_H */
