#include <math.h>
#include "krand.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif /* M_PI */

__thread kstate _ds = {
	{
		15825392874305938225ull,
		16955680563785373609ull,
		7795344188541920541ull,
		15657919877309493704ull
	}
};

__thread kstate32 _ds32 = {
	{
    1777456429,
    3007012567,
    4219636203,
    1433776259
	}
};

__thread kstate _dsf = {
	{
		16955680563785373609ull,
		7795344188541920541ull,
		15657919877309493704ull,
		15825392874305938225ull
	}
};

static u64 
splitmix64(u64 *s) {
	u64 r;

	r = (*s += 0x9e3779b97f4a7c15);
	r = (r ^ (r >> 30)) * 0xbf58476d1ce4e5b9;
	r = (r ^ (r >> 27)) * 0x94d049bb133111eb;
	r ^= (r >> 31);

	return r;
}

void 
kseed_r(kstate* s, u64 n)
{
	s->s[0] = splitmix64(&n);
	s->s[1] = splitmix64(&n);
	s->s[2] = splitmix64(&n);
	s->s[3] = splitmix64(&n);
}

void 
kseed32_r(kstate32* s, u32 n)
{
	u64 _s;

	_s = n;
	u64 t = splitmix64(&_s); 
	s->s[0] = (u32)t;
	s->s[1] = (u32)(t >> 32);
	t = splitmix64(&_s); 
	s->s[0] = (u32)t;
	s->s[1] = (u32)(t >> 32);
}

u64 
krand_r(kstate* s)
{
	u64 r = lrot64(s->s[1] * 5, 7) * 9;
	u64 t = s->s[1] << 17;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;
	s->s[3] = lrot64(s->s[3], 45);

	return r;
}

u64
krand_range_r(kstate* s, u64 h)
{
	u64 t, x;

	// if h is a power of 2
	if ((h & (h - 1)) == 0)
		return krand_r(s) & (h - 1);

	// take top rejection range instead of bottom
	t = (u64)-1 - (-h % h);

	do {
		x = krand_r(s);
	} while (x > t);

	return x % h;
}

u32 
krand32_r(kstate32* s)
{
	u32 r = lrot32(s->s[1] * 5, 7) * 9;
	u32 t = s->s[1] << 9;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;

	s->s[3] = lrot32(s->s[3], 11);

	return r;
}

u32 
krand32_range_r(kstate32* s, u32 r)
{
	u32 t, x, l;
	u64 m;

	/* integer method */
	t = -r % r;

	do {
		x = krand32_r(s);
		m = (u64)x * (u64)r;
		l = (u32)m;
	} while (l < t);

	return m >> 32;
}

void
kseed(u64 n)
{
    kseed_r(&_ds, n);
    kseed_r(&_dsf, n + 4118897797544179723ull);
}

void
kseed32(u32 n)
{
    kseed32_r(&_ds32, n);
}

u64 
krand(void)
{
    return krand_r(&_ds);
}

u64 
krand_range(u64 h)
{
    return krand_range_r(&_ds, h);
}

u32 
krand32(void)
{
    return krand32_r(&_ds32);
}

u32 
krand32_range(u32 h)
{
    return krand32_range_r(&_ds32, h);
}

double
krandf_r(kstate* s)
{
	u64 r = s->s[0] + s->s[3];
	u64 t = s->s[1] << 17;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;
	s->s[3] = lrot64(s->s[3], 45);

	return (r >> 11) * 0x1.0p-53;
}

double 
krandf(void)
{
	kstate *s = &_dsf;
	u64 r = s->s[0] + s->s[3];
	u64 t = s->s[1] << 17;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;
	s->s[3] = lrot64(s->s[3], 45);

	return (r >> 11) * 0x1.0p-53;
}

int
k_bernoulli_r(kstate *s, double p)
{
	if (p < 0.0)
		p = 0.0;
	if (p > 1.0)
		p = 1.0;

	return krandf_r(s) < p;
}

int
k_bernoulli(double p)
{
	return k_bernoulli_r(&_dsf, p);
}

#ifdef KRAND_DISTRIBUTIONS
double
k_chisq_r(kstate *s, int k)
{
	double x, sum;
	int i;

	sum = 0.0;
	for (i = 0; i < k; i++) {
		x = k_normal_r(s, 0.0, 1.0);
		sum += x * x;
	}

	return sum;
}

double
k_normal_r(kstate *s, double mean, double sd)
{
	double u1 = krandf_r(s);
	double u2 = krandf_r(s);
	double z = sqrt(-2.0 * log(u1)) * cos(2.0 * M_PI * u2);

	return mean + sd * z;
}

double
k_exponential_r(kstate *s, double lambda)
{
	double u = krandf_r(s);

	return -log(u) / lambda;
}

int
k_poisson_r(kstate *s, double lambda)
{
	double u, p, L;
	int k = 0;

	p = 1.0;
	L = exp(-lambda);
	while (p > L) {
		k++;
		u = krandf_r(s);
		p *= u;
	}

	return k - 1;
}

double
k_gamma_r(kstate *s, double alpha, double beta)
{
	double u, d, c, z, v, x;

	if (alpha <= 0.0 || beta <= 0.0) {
		return NAN;
	}

	if (alpha < 1.0) {
		u = krandf_r(s);

		return k_gamma_r(s, 1.0 + alpha, beta) * pow(u, 1.0 / alpha);
	}

	d = alpha - 1.0 / 3.0;
	c = 1.0 / sqrt(9.0 * d);
	z = k_normal_r(s, 0.0, 1.0);
	v = 1.0 + c * z;
	x = v * v * v;
	if (x <= 0.0) {
		return NAN;
	}

	u = krandf_r(s);
	if (u < 1.0 - 0.0331 * z * z * z * z) {
		return beta * d * x;
	}

	return beta * (-log(krandf_r(s)) / d);

}

double
k_weibull_r(kstate *s, double lambda, double k)
{
	double u = krandf_r(s);

	return lambda * pow(-log(1.0 - u), 1.0 / k);
}

double
k_beta_r(kstate *s, double alpha, double beta)
{
	double gamma1, gamma2;

	gamma1 = k_gamma_r(s, alpha, 1.0);
	gamma2 = k_gamma_r(s, beta, 1.0);

	return gamma1 / (gamma1 + gamma2);
}

double
k_chisq(int k)
{
	return k_chisq_r(&_dsf, k);
}

double
k_normal(double mean, double sd)
{
	return k_normal_r(&_dsf, mean, sd);
}

double
k_exponential(double lambda)
{
	return k_exponential_r(&_dsf, lambda);
}

int
k_poisson(double lambda)
{
	return k_poisson_r(&_dsf, lambda);
}

double
k_gamma(double alpha, double beta)
{
	return k_gamma_r(&_dsf, alpha, beta);
}

double
k_weibull(double lambda, double k)
{
	return k_weibull_r(&_dsf, lambda, k);
}

double
k_beta(double alpha, double beta)
{
	return k_beta_r(&_dsf, alpha, beta);
}
#endif /* KRAND_DISTRIBUTIONS */

